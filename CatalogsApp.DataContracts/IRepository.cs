﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CatalogsApp.DataEntities;

namespace CatalogsApp.DataContracts
{
    public interface IRepository<TEntity>
        where TEntity : IEntity
    {
        Task<TEntity> FirstOrDefaultAsync(int id, CancellationToken cancellationToken);
        IQueryable<TEntity> GetAll();
        Task<int> SaveAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
        void CommitChanges();
    }
}
