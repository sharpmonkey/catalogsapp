﻿using CatalogsApp.DataEntities.Entities.Products;

namespace CatalogsApp.DataContracts.Products
{
    public interface IProductCatalogRepository : IRepository<ProductCatalog>
    {
    }
}
