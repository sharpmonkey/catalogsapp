﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CatalogsApp.DataEntities.Entities.Products
{
    public class ProductCatalog : IEntity
    {
        public int Id { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

        [Required]
        [StringLength(255)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [Range(0, Double.PositiveInfinity)]
        public decimal Price { get; set; }

        public DateTime? LastUpdated { get; set; }

        public byte[] Photo { get; set; }

        public bool IsConfirmed { get; set; }
    }
}
