﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CatalogApp.Business.Products;
using CatalogApp.Business.Products.Queries;
using CatalogsApp.Api.ModelAdapters.Products;
using CatalogsApp.Api.Models.Products;

namespace CatalogsApp.Api.Controllers.Products
{
    public sealed class ProductCatalogsController : CommonApiController
    {
        private IProductCatalogService ProductCatalogService { get; }

        private IProductCatalogModelAdapter ProductCatalogModelAdapter { get; }

        public ProductCatalogsController(IProductCatalogService productCatalogService, IProductCatalogModelAdapter productCatalogModelAdapter)
        {
            ProductCatalogService = productCatalogService;
            ProductCatalogModelAdapter = productCatalogModelAdapter;
        }

        [Route("api/product_catalogs/{id:int}")]
        public async Task<HttpResponseMessage> Get([FromUri(Name = "id")]int id)
        {
            HttpResponseMessage response;
            var cancellationToken = GetCancellationToken();

            var productCatalog = await ProductCatalogService.GetProductCatalogByIdAsync(id, cancellationToken);

            if (productCatalog != null)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, await ProductCatalogModelAdapter.ToModelAsync(productCatalog, cancellationToken));
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return response;
        }

        [Route("api/product_catalogs")]
        public async Task<HttpResponseMessage> Get([FromUri]GetProductCatalogsQuery query)
        {
            HttpResponseMessage response;
            var cancellationToken = GetCancellationToken();

            var productCatalogs = await ProductCatalogService.FindProductCatalogsAsync(query, cancellationToken);

            if (productCatalogs?.Count > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, await ProductCatalogModelAdapter.ToModelsAsync(productCatalogs, cancellationToken));

                var totalItemCount = await ProductCatalogService.FindProductCatalogsTotalCountAsync(query, cancellationToken);
                response.Headers.Add("X-Total-Count", totalItemCount.ToString());
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
            }

            return response;
        }

        [Route("api/product_catalogs")]
        public async Task<HttpResponseMessage> Post([FromBody]ProductCatalogModel model)
        {
            HttpResponseMessage response;

            if (model != null && ModelState.IsValid)
            {
                var cancellationToken = GetCancellationToken();

                try
                {
                    var id = await ProductCatalogService.SaveProductCatalogAsync(await ProductCatalogModelAdapter.ToDocumentAsync(model, cancellationToken), cancellationToken);

                    response = Request.CreateResponse(HttpStatusCode.Created);

                    AddLocation(response, "api/product_catalogs/{0}", id);
                }
                catch (ProductCatalogServiceValidationException ex)
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, new
                    {
                        ex.Message
                    });
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, new
                {
                    Message = "Parameters are invalid",
                    Errors = GetModelErrors()
                });
            }

            return response;
        }

        [Route("api/product_catalogs/{id:int}")]
        public async Task<HttpResponseMessage> Put([FromUri(Name = "id")]int id, [FromBody]ProductCatalogModel model)
        {
            HttpResponseMessage response;

            var productCatalog = await ProductCatalogModelAdapter.ToDocumentAsync(model, GetCancellationToken());
            if (productCatalog != null)
            {
                if (model != null && ModelState.IsValid)
                {
                    try
                    {
                        await ProductCatalogService.UpdateProductCatalogAsync(productCatalog, GetCancellationToken());

                        response = Request.CreateResponse(HttpStatusCode.NoContent);
                    }
                    catch (ProductCatalogServiceValidationException ex)
                    {
                        response = Request.CreateResponse(HttpStatusCode.BadRequest, new
                        {
                            ex.Message
                        });
                    }
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, new
                    {
                        Message = "Parameters are invalid",
                        Errors = GetModelErrors()
                    });
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return response;
        }

        [Route("api/product_catalogs/{id:int}")]
        public async Task<HttpResponseMessage> Delete([FromUri(Name = "id")]int id)
        {
            var cancellationToken = GetCancellationToken();
            var response = Request.CreateResponse(HttpStatusCode.NotFound);

            try
            {
                var productCatalog = await ProductCatalogService.GetProductCatalogByIdAsync(id, cancellationToken);
                if (productCatalog != null)
                {
                    await ProductCatalogService.DeleteProductCatalogAsync(productCatalog, cancellationToken);

                    response = Request.CreateResponse(HttpStatusCode.NoContent);
                }
            }
            catch (ProductCatalogServiceValidationException ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, new
                {
                    ex.Message
                });
            }

            return response;
        }
    }
}
