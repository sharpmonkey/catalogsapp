﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace CatalogsApp.Api.Controllers
{
    public class CommonApiController : ApiController
    {
        public static CancellationToken GetCancellationToken()
        {
            return CancellationTokenSource.CreateLinkedTokenSource(new[]
            {
                HttpContext.Current.Request.TimedOutToken,
                HttpContext.Current.Response.ClientDisconnectedToken
            }).Token;
        }

        public IEnumerable<dynamic> GetModelErrors()
        {
            var errors = new List<dynamic>();

            foreach (var propertyName in ModelState.Keys)
            {
                if (ModelState[propertyName].Errors != null && ModelState[propertyName].Errors.Count > 0)
                {
                    errors.Add(new
                    {
                        PropertyName = ToCamelCased(propertyName),
                        Message = string.Join(". ", ModelState[propertyName].Errors.Select(x => x.ErrorMessage))
                    });
                }
            }

            return errors;
        }

        public string ToCamelCased(string value)
        {
            var output = new StringBuilder();

            var fragments = value.Split(new[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < fragments.Length; i++)
            {
                if (i != 0)
                {
                    output.Append(fragments[i].Substring(0, 1).ToUpperInvariant());
                    output.Append(fragments[i].Substring(1));
                }
                else
                {
                    output.Append(fragments[i].Substring(0, 1).ToLowerInvariant());
                    output.Append(fragments[i].Substring(1));
                }
            }

            return output.ToString();
        }

        protected void AddLocation(HttpResponseMessage response, string format, params object[] args)
        {
            response.Headers.Location = GetLocationUri(string.Format(format, args));
        }

        protected Uri GetLocationUri(string path)
        {
            var port = !Url.Request.RequestUri.IsDefaultPort ? $":{Url.Request.RequestUri.Port}" : string.Empty;

            return new Uri(Url.Request.RequestUri.Scheme + "://" + Url.Request.RequestUri.Host.TrimEnd('/') + port + "/" + path.TrimStart('/'));
        }
    }
}
