﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using CatalogApp.Business.Products;
using CatalogApp.Business.Products.Queries;

namespace CatalogsApp.Api.Controllers.Exchange
{
    public sealed class DataExchangeController : CommonApiController
    {
        private IProductCatalogService ProductCatalogService { get; }

        public DataExchangeController(IProductCatalogService productCatalogService)
        {
            ProductCatalogService = productCatalogService;
        }

        [Route("api/data_exchange")]
        public async Task<HttpResponseMessage> Get([FromUri]GetProductCatalogsQuery query)
        {
            HttpResponseMessage response = null;
            var cancellationToken = GetCancellationToken();

            try
            {
                var outputFile = await ProductCatalogService.FindAndExportProductCatalogsAsync(query, cancellationToken);

                if (outputFile == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }

                response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StreamContent(outputFile),
                };

                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "product_catalogs.xlsx"
                };

                response.Headers.CacheControl = new CacheControlHeaderValue
                {
                    Private = true,
                    MaxAge = new TimeSpan(1, 0, 0)
                };
            }
            catch (ProductCatalogServiceValidationException ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, new
                {
                    ex.Message
                });
            }

            return response;
        }
    }
}
