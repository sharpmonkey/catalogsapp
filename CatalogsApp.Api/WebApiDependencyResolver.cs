﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;

namespace CatalogsApp.Api
{
    public sealed class WebApiDependencyResolver : IDependencyResolver
    {
        private IServiceProvider ServiceProvider { get; }

        private IServiceScopeFactory ServiceScopeFactory { get; set; }

        public WebApiDependencyResolver(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public IDependencyScope BeginScope()
        {
            if (ServiceScopeFactory == null)
            {
                ServiceScopeFactory = ServiceProvider.GetRequiredService<IServiceScopeFactory>();
            }

            return new WebApiDependencyScope(ServiceScopeFactory.CreateScope());
        }

        public object GetService(Type serviceType)
        {
            return ServiceProvider.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return ServiceProvider.GetServices(serviceType);
        }

        public void Dispose()
        {

        }
    }
}
