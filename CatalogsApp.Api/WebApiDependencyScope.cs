﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;

namespace CatalogsApp.Api
{
    public sealed class WebApiDependencyScope : IDependencyScope
    {
        private IServiceScope ServiceScope { get; }

        public WebApiDependencyScope(IServiceScope serviceScope)
        {
            ServiceScope = serviceScope;
        }

        public object GetService(Type serviceType)
        {
            return ServiceScope.ServiceProvider.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return ServiceScope.ServiceProvider.GetServices(serviceType);
        }

        public void Dispose()
        {
            ServiceScope.Dispose();
        }
    }
}
