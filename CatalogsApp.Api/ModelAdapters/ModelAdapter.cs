﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CatalogsApp.Api.ModelAdapters
{
    public class ModelAdapter<TDocument, TModel> : IModelAdapter<TDocument, TModel>
        where TDocument : class
        where TModel : class, new()
    {
        public virtual ValueTask<TDocument> ToDocumentAsync(TModel model, CancellationToken cancelationToken)
        {
            return model != null ? new ValueTask<TDocument>(CopyProperties<TModel, TDocument>(model)) : new ValueTask<TDocument>();
        }

        public virtual ValueTask<TModel> ToModelAsync(TDocument document, CancellationToken cancelationToken)
        {
            return document != null ? new ValueTask<TModel>(CopyProperties<TDocument, TModel>(document)) : new ValueTask<TModel>();
        }

        public virtual async ValueTask<IList<TModel>> ToModelsAsync(IList<TDocument> documents, CancellationToken cancelationToken)
        {
            var models = new List<TModel>();

            if (documents?.Count > 0)
            {
                foreach (var entity in documents)
                {
                    models.Add(await ToModelAsync(entity, cancelationToken));
                }
            }

            return models;
        }

        protected virtual TTarget CopyProperties<TSource, TTarget>(TSource source)
        {
            var target = Activator.CreateInstance<TTarget>();

            var targetProperties = typeof(TTarget).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty);
            foreach (var targetProperty in targetProperties)
            {
                var sourceProperty = typeof(TSource).GetProperty(targetProperty.Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty);
                if (sourceProperty != null)
                {
                    targetProperty.SetValue(target, sourceProperty.GetValue(source));
                }
            }

            return target;
        }
    }
}
