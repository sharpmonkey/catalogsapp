﻿using CatalogsApp.Api.Models.Products;
using CatalogsApp.DataEntities.Entities.Products;

namespace CatalogsApp.Api.ModelAdapters.Products
{
    public class ProductCatalogModelAdapter : ModelAdapter<ProductCatalog, ProductCatalogModel>, IProductCatalogModelAdapter
    {
    }
}
