﻿using CatalogsApp.Api.Models.Products;
using CatalogsApp.DataEntities.Entities.Products;

namespace CatalogsApp.Api.ModelAdapters.Products
{
    public interface IProductCatalogModelAdapter : IModelAdapter<ProductCatalog, ProductCatalogModel>
    {
    }
}
