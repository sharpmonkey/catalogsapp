﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CatalogsApp.Api.ModelAdapters
{
    public interface IModelAdapter<TDocument, TModel>
        where TDocument : class
        where TModel : class, new()
    {
        ValueTask<TDocument> ToDocumentAsync(TModel model, CancellationToken cancelationToken);
        ValueTask<TModel> ToModelAsync(TDocument document, CancellationToken cancelationToken);
        ValueTask<IList<TModel>> ToModelsAsync(IList<TDocument> documents, CancellationToken cancelationToken);
    }
}
