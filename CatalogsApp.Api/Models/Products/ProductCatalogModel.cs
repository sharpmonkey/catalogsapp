﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CatalogsApp.Api.Models.Products
{
    public class ProductCatalogModel
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "Code is required")]
        public string Code { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage = "Price is required")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Price must be greater than zero")]
        public decimal Price { get; set; }

        public DateTime? LastUpdated { get; set; }

        public byte[] Photo { get; set; }
    }
}
