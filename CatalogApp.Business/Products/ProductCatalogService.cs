﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CatalogApp.Business.Products.Queries;
using CatalogApp.Business.Readers;
using CatalogsApp.DataContracts.Products;
using CatalogsApp.DataEntities.Entities.Products;

namespace CatalogApp.Business.Products
{
    public class ProductCatalogService : ServiceBase, IProductCatalogService
    {
        private IProductCatalogRepository ProductCatalogRepository { get; }

        public ProductCatalogService(IProductCatalogRepository productCatalogRepository)
        {
            ProductCatalogRepository = productCatalogRepository;
        }

        public async Task<IList<ProductCatalog>> FindProductCatalogsAsync(GetProductCatalogsQuery query, CancellationToken cancellationToken)
        {
            IList<ProductCatalog> results;

            if (query == null)
            {
                query = new GetProductCatalogsQuery();
            }

            try
            {
                results = GetProductCatalogQuery(query)
                    .Skip(query.Skip ?? 0)
                    .Take(query.Limit ?? 20)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw new ProductCatalogServiceException("Failed to fetch product catalogs", ex);
            }

            return await Task.FromResult(results);
        }

        public async Task<int> FindProductCatalogsTotalCountAsync(GetProductCatalogsQuery query, CancellationToken cancellationToken)
        {
            int totalCount;

            if (query == null)
            {
                query = new GetProductCatalogsQuery();
            }

            try
            {
                totalCount = GetProductCatalogQuery(query)
                    .Count();
            }
            catch (Exception ex)
            {
                throw new ProductCatalogServiceException("Failed to fetch product catalogs total count", ex);
            }

            return await Task.FromResult(totalCount);
        }

        public async Task<ProductCatalog> GetProductCatalogByIdAsync(int id, CancellationToken cancellationToken)
        {
            ProductCatalog productCatalog;

            try
            {
                productCatalog = await ProductCatalogRepository.FirstOrDefaultAsync(id, cancellationToken);

                if (productCatalog == null || productCatalog.IsDeleted)
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new ProductCatalogServiceException("Failed to get product catalog", ex);
            }

            return productCatalog;
        }

        public async Task<int> SaveProductCatalogAsync(ProductCatalog productCatalog, CancellationToken cancellationToken)
        {
            int result;

            if (productCatalog == null)
            {
                throw new ProductCatalogServiceValidationException("Product catalog is not set");
            }

            if (string.IsNullOrEmpty(productCatalog.Code))
            {
                throw new ProductCatalogServiceValidationException("Product catalog code is not set");
            }

            try
            {
                var exists = ProductCatalogRepository.GetAll().Any(x => x.Code == productCatalog.Code && !x.IsDeleted);

                if (exists)
                {
                    throw new ProductCatalogServiceValidationException("Product catalog code is not unique");
                }

                if (productCatalog.Price < 1000M)
                {
                    productCatalog.IsConfirmed = true;
                }

                productCatalog.CreatedOn = DateTime.UtcNow;

                await ProductCatalogRepository.SaveAsync(productCatalog);

                ProductCatalogRepository.CommitChanges();

                result = productCatalog.Id;
            }
            catch (Exception ex)
            {
                throw new ProductCatalogServiceException("Failed to save product catalog", ex);
            }

            return result;
        }

        public async Task UpdateProductCatalogAsync(ProductCatalog productCatalog, CancellationToken cancellationToken)
        {
            if (productCatalog == null)
            {
                throw new ProductCatalogServiceValidationException("Product catalog is not set");
            }

            if (string.IsNullOrEmpty(productCatalog.Code))
            {
                throw new ProductCatalogServiceValidationException("Product catalog code is not set");
            }

            try
            {
                var exists = ProductCatalogRepository.GetAll().Any(x => x.Code == productCatalog.Code && !x.IsDeleted && x.Id != productCatalog.Id);

                if (exists)
                {
                    throw new InvalidOperationException("Product catalog code is not unique");
                }

                if (productCatalog.Price < 1000M)
                {
                    productCatalog.IsConfirmed = true;
                }

                productCatalog.CreatedOn = DateTime.UtcNow;

                await ProductCatalogRepository.SaveAsync(productCatalog);

                ProductCatalogRepository.CommitChanges();
            }
            catch (Exception ex)
            {
                throw new ProductCatalogServiceException("Failed to update product catalog", ex);
            }
        }

        public async Task DeleteProductCatalogAsync(ProductCatalog productCatalog, CancellationToken cancellationToken)
        {
            if (productCatalog == null)
            {
                throw new ProductCatalogServiceValidationException("Product catalog is not set");
            }

            try
            {
                productCatalog.IsDeleted = true;

                await ProductCatalogRepository.SaveAsync(productCatalog);

                ProductCatalogRepository.CommitChanges();
            }
            catch (Exception ex)
            {
                throw new ProductCatalogServiceException("Failed to delete product catalog", ex);
            }
        }

        public async Task<Stream> FindAndExportProductCatalogsAsync(GetProductCatalogsQuery query, CancellationToken cancellationToken)
        {
            Stream outputStream = null;
            
            try
            {
                var productCatalogs = await FindProductCatalogsAsync(query, cancellationToken);

                if (productCatalogs?.Count > 0)
                {
                    outputStream = await WriteToStream(productCatalogs, cancellationToken);
                }
            }
            catch (Exception ex)
            {
                throw new ProductCatalogServiceException("Failed to export product catalog", ex);
            }

            return outputStream;
        }

        private async Task<Stream> WriteToStream(IList<ProductCatalog> productCatalogs, CancellationToken cancellationToken)
        {
            Stream result = null;

            if (productCatalogs?.Count > 0)
            {
                var dataWriter = new XlsxDataWriter
                {
                    Header = new List<string> { "Code", "Name", "Price", "LastUpdated", "CreatedOn" }
                };

                await dataWriter.InitializeAsync(cancellationToken);

                foreach (var productCatalog in productCatalogs)
                {
                    await dataWriter.WriteAsync(ToDictionary(productCatalog), cancellationToken);
                }

                result = dataWriter.Stream;
                await dataWriter.CloseAsync(cancellationToken);
            }

            return result;
        }

        private IQueryable<ProductCatalog> GetProductCatalogQuery(GetProductCatalogsQuery query)
        {
            var queryable = ProductCatalogRepository
                .GetAll()
                .Where(c => !c.IsDeleted);

            if (!string.IsNullOrEmpty(query.Keyword))
            {
                queryable = queryable.Where(c => (c.Code.ToLower().Contains(query.Keyword.ToLower()) || c.Name.Contains(query.Keyword.ToLower())));
            }

            if (query.From.HasValue)
            {
                queryable = queryable.Where(c => c.CreatedOn >= query.From.Value);
            }

            if (query.To.HasValue)
            {
                queryable = queryable.Where(c => c.CreatedOn <= query.To.Value);
            }

            var sortBy = "Id";
            if (!string.IsNullOrEmpty(query.Sort))
            {
                sortBy = query.Sort;
            }

            queryable = queryable
                .OrderBy(sortBy);

            return queryable;
        }

        private static IDictionary<string, object> ToDictionary(ProductCatalog productCatalog)
        {
            dynamic result = new ExpandoObject();
            result.Code = productCatalog.Code;
            result.Name = productCatalog.Name;
            result.Price = productCatalog.Price;
            result.LastUpdated = productCatalog.LastUpdated;
            result.CreatedOn = productCatalog.CreatedOn;
            return result;
        }
    }
}
