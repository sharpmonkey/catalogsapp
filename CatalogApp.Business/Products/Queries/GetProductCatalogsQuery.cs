﻿using System;

namespace CatalogApp.Business.Products.Queries
{
    public class GetProductCatalogsQuery
    {
        public string Keyword { get; set; }

        public DateTime? From  { get; set; }

        public DateTime? To { get; set; }

        public string Sort { get; set; }

        public int? Skip { get; set; }

        public int? Limit { get; set; }
    }
}
