﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CatalogApp.Business.Products.Queries;
using CatalogsApp.DataEntities.Entities.Products;

namespace CatalogApp.Business.Products
{
    public interface IProductCatalogService
    {
        Task<IList<ProductCatalog>> FindProductCatalogsAsync(GetProductCatalogsQuery query, CancellationToken cancellationToken);
        Task<int> FindProductCatalogsTotalCountAsync(GetProductCatalogsQuery query, CancellationToken cancellationToken);
        Task<ProductCatalog> GetProductCatalogByIdAsync(int id, CancellationToken cancellationToken);
        Task<int> SaveProductCatalogAsync(ProductCatalog productCatalog, CancellationToken cancellationToken);
        Task UpdateProductCatalogAsync(ProductCatalog productCatalog, CancellationToken cancellationToken);
        Task DeleteProductCatalogAsync(ProductCatalog productCatalog, CancellationToken cancellationToken);
        Task<Stream> FindAndExportProductCatalogsAsync(GetProductCatalogsQuery query, CancellationToken cancellationToken);
    }
}
