﻿using System;

namespace CatalogApp.Business.Products
{
    public class ProductCatalogServiceValidationException : Exception
    {
        public ProductCatalogServiceValidationException(string message)
    : base(message)
        {
        }

        public ProductCatalogServiceValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
