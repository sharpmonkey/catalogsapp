﻿using System;

namespace CatalogApp.Business.Products
{
    public class ProductCatalogServiceException : Exception
    {
        public ProductCatalogServiceException(string message)
    : base(message)
        {
        }

        public ProductCatalogServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
