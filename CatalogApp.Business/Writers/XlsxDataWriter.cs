﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace CatalogApp.Business.Readers
{
    public class XlsxDataWriter : IDisposable
    {
        public IList<string> Header { get; set; }

        public string DateTimeFormat { get; set; }

        public Stream Stream { get; set; }

        private bool disposed;

        private ExcelPackage excelPackage;

        private ExcelWorksheet excelWorksheet;

        private int position;

        private bool headerWritten;

        public Task<int> WriteAsync(IDictionary<string, object> item, CancellationToken cancellationToken)
        {
            if (Header == null)
            {
                Header = new List<string>(item.Keys.Count);

                foreach (var key in item.Keys)
                {
                    Header.Add(key);
                }
            }

            if (position == 1 && !headerWritten)
            {
                WriteHeader();
            }

            var collumnIndex = 1;
            foreach (var headerItem in Header)
            {
                item.TryGetValue(headerItem, out var value);

                var cell = excelWorksheet.Cells[position, collumnIndex];

                cell.Value = value;

                if (value is DateTime)
                {
                    cell.Style.Numberformat.Format = DateTimeFormat;
                }

                collumnIndex++;
            }

            position++;

            return Task.FromResult(0);
        }

        public async Task CloseAsync(CancellationToken cancellationToken)
        {
            excelPackage.Save();

            if (Stream != null)
            {
                Stream.Position = 0;
            }
        }

        public async Task InitializeAsync(CancellationToken cancellationToken)
        {
            if (Stream == null)
            {
                Stream = new MemoryStream();
            }

            if (excelPackage == null)
            {
                excelPackage = new ExcelPackage(Stream);
            }

            if (excelWorksheet == null)
            {
                excelPackage.Workbook.Worksheets.Add("Result");
                excelWorksheet = excelPackage.Workbook.Worksheets["Result"];
            }

            if (string.IsNullOrEmpty(DateTimeFormat))
            {
                DateTimeFormat = "m/d/yyyy";
            }

            position = 1;

            if (Header != null && !headerWritten)
            {
                WriteHeader();
            }
        }

        private void WriteHeader()
        {
            var collumnIndex = 1;
            foreach (var headerItem in Header)
            {
                var cell = excelWorksheet.Cells[position, collumnIndex];
                cell.Value = headerItem;
                cell.Style.Font.Bold = true;
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                collumnIndex++;
            }

            headerWritten = true;
            position++;
        }

        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                excelPackage = null;
                excelWorksheet = null;
                Header = null;

                disposed = true;
            }
        }

        public void Dispose()
        {
            if (!disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
    }
}
