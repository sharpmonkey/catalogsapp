﻿using CatalogApp.Business.Products;
using CatalogsApp.Api.Controllers.Exchange;
using CatalogsApp.Api.Controllers.Products;
using CatalogsApp.Api.ModelAdapters.Products;
using CatalogsApp.Data;
using CatalogsApp.Data.Products;
using CatalogsApp.DataContracts.Products;
using Microsoft.Extensions.DependencyInjection;
using System.Configuration;
using System.Web.Mvc;

namespace CatalogsApp.Web
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["CatalogsApp"].ConnectionString;

            services
                .AddScoped<IProductCatalogRepository>(x => new ProductCatalogRepository(
                    new CatalogsAppDbContext(connectionString)
                ))
                .AddScoped<IProductCatalogService>(x => new ProductCatalogService(
                    x.GetService<IProductCatalogRepository>()
                ))
                .AddScoped<IProductCatalogModelAdapter>(x => new ProductCatalogModelAdapter())
                .AddTransient<ProductCatalogsController>()
                .AddTransient<DataExchangeController>();
        }
    }
}