﻿using CatalogsApp.Api;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CatalogsApp.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static IServiceProvider ServiceProvider { get; set; }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var services = new ServiceCollection();

            new Startup().ConfigureServices(services);

            GlobalConfiguration.Configuration.DependencyResolver = new WebApiDependencyResolver(services.BuildServiceProvider());

            ServicePointManager.DefaultConnectionLimit = 100;
            ServicePointManager.MaxServicePointIdleTime = 300000;
            ServicePointManager.UseNagleAlgorithm = false;
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.SetTcpKeepAlive(true, 1000, 100);

            RegisterMvcRoutes(RouteTable.Routes);

            GlobalConfiguration.Configuration.EnsureInitialized();
        }

        private static void RegisterMvcRoutes(RouteCollection routes)
        {
            routes.MapRoute("api_search", "api/{controller}", new { action = "search", area = "" }, new { httpMethod = new HttpMethodConstraint("GET") });
            routes.MapRoute("api_default", "api/{controller}/{id}/{action}", new { action = "default", area = "", id = UrlParameter.Optional }, new { id = "^[0-9]*$" });
        }
    }
}
