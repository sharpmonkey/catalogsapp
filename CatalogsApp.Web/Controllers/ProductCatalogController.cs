﻿using CatalogApp.Business.Products.Queries;
using CatalogsApp.Api.Models.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using PagedList;

namespace CatalogsApp.Web.Controllers
{
    public class ProductCatalogController : Controller
    {
        private static readonly int PageSize = 20;

        public async Task<ViewResult> Index(string sortBy, string currentFilter, string keyword, int? page)
        {
            IPagedList<ProductCatalogModel> pagedList = null;
            var totalItemCount = 0;

            if (keyword != null)
            {
                page = 1;
            }
            else
            {
                keyword = currentFilter;
            }

            ViewBag.CurrentSort = sortBy;
            ViewBag.CurrentFilter = keyword;

            var query = new GetProductCatalogsQuery
            {
                Keyword = keyword,
                Sort = sortBy,
                Skip = (page - 1) * PageSize,
                Limit = PageSize,
            };

            IList<ProductCatalogModel> productCatalogs = null;

            using (var client = GetApiClient())
            {
                var queryString = GetSearchQueryString(query);
                var apiResponse = await client.GetAsync("product_catalogs" + queryString);

                if (apiResponse.IsSuccessStatusCode)
                {
                    productCatalogs = await apiResponse.Content.ReadAsAsync<IList<ProductCatalogModel>>();

                    if (apiResponse.Headers.Contains("X-Total-Count"))
                    {
                        totalItemCount = int.Parse(apiResponse.Headers.GetValues("X-Total-Count").First());
                    }

                    ViewBag.ExportLink = GetExportLink(query);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error. Please contact system administrator.");
                }
            }

            if (productCatalogs?.Count > 0)
            {
                pagedList = new StaticPagedList<ProductCatalogModel>(productCatalogs, page ?? 1, query.Limit.Value, totalItemCount);
            }

            return View(pagedList);
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ProductCatalogModel productCatalog = null;

            using (var client = GetApiClient())
            {
                var apiResponse = await client.GetAsync($"product_catalogs/{id.Value}");

                if (apiResponse.IsSuccessStatusCode)
                {
                    productCatalog = await apiResponse.Content.ReadAsAsync<ProductCatalogModel>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error. Please contact system administrator.");
                }
            }

            if (productCatalog == null)
            {
                return HttpNotFound();
            }

            return View(productCatalog);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ProductCatalogModel model)
        {
            if (ModelState.IsValid)
            {
                using (var client = GetApiClient())
                {
                    var apiResponse = await client.PostAsXmlAsync($"product_catalogs", model);

                    if (!apiResponse.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError(string.Empty, "Server error. Please contact system administrator.");
                    }
                }
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var productCatalog = await GetProductCatalogAsync(id.Value);

            if (productCatalog == null)
            {
                return HttpNotFound();
            }

            return View(productCatalog);
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var productCatalog = await GetProductCatalogAsync(id.Value);

            if (TryUpdateModel(productCatalog, "",
               new string[] { "Code", "Name", "Price", "LastUpdated" }))
            {
                using (var client = GetApiClient())
                {
                    var apiResponse = await client.PutAsXmlAsync($"product_catalogs/{id}", productCatalog);

                    if (apiResponse.IsSuccessStatusCode)
                    {
                        await apiResponse.Content.ReadAsAsync<ProductCatalogModel>();
                    }
                    else
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Server error. Please contact system administrator.");
                    }
                }

                return RedirectToAction("Index");
            }

            return View(productCatalog);
        }

        public async Task<ActionResult> Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persists see your system administrator.";
            }

            var productCatalog = await GetProductCatalogAsync(id.Value);

            if (productCatalog == null)
            {
                return HttpNotFound();
            }

            return View(productCatalog);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id)
        {
            var productCatalog = await GetProductCatalogAsync(id);

            if (productCatalog == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            using (var client = GetApiClient())
            {
                var apiResponse = await client.DeleteAsync($"product_catalogs/{id}");

                if (!apiResponse.IsSuccessStatusCode)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Server error. Please contact system administrator.");
                }
            }

            return RedirectToAction("Index");
        }

        private async Task<ProductCatalogModel> GetProductCatalogAsync(int id)
        {
            ProductCatalogModel productCatalog = null;

            using (var client = GetApiClient())
            {
                var apiResponse = await client.GetAsync($"product_catalogs/{id}");

                if (apiResponse.IsSuccessStatusCode)
                {
                    productCatalog = await apiResponse.Content.ReadAsAsync<ProductCatalogModel>();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server error. Please contact system administrator.");
                }
            }

            return productCatalog;
        }

        private HttpClient GetApiClient()
        {
            var client = new HttpClient();
            var apiUrl = HttpContext.Request.Url.AbsoluteUri.Replace(this.HttpContext.Request.Url.PathAndQuery, "") + "/api/";
            client.BaseAddress = new Uri(apiUrl);

            return client;
        }

        private static string GetExportLink(GetProductCatalogsQuery query)
        {
            var exportPath = "/api/data_exchange";
            var exportQuery = GetSearchQueryString(query);

            if (exportQuery != null)
            {
                exportPath += exportQuery;
            }

            return exportPath;
        }

        private static string GetSearchQueryString(GetProductCatalogsQuery query)
        {
            string searchQueryString = null;

            if (!string.IsNullOrEmpty(query.Keyword))
            {
                searchQueryString = $"keyword={query.Keyword}";
            }

            if (query.From.HasValue)
            {
                if (searchQueryString != null)
                {
                    searchQueryString += "&";
                }

                searchQueryString += $"from={query.From.Value}";
            }

            if (query.To.HasValue)
            {
                if (searchQueryString != null)
                {
                    searchQueryString += "&";
                }

                searchQueryString += $"to={query.To.Value}";
            }

            if (query.Skip.HasValue)
            {
                if (searchQueryString != null)
                {
                    searchQueryString += "&";
                }

                searchQueryString += $"skip={query.Skip.Value}";
            }

            if (query.Limit.HasValue)
            {
                if (searchQueryString != null)
                {
                    searchQueryString += "&";
                }

                searchQueryString += $"limit={query.Limit.Value}";
            }

            if (!string.IsNullOrEmpty(query.Sort))
            {
                if (searchQueryString != null)
                {
                    searchQueryString += "&";
                }

                searchQueryString += $"sort={query.Sort}";
            }

            if (searchQueryString != null)
            {
                searchQueryString = $"?{searchQueryString}";
            }

            return searchQueryString;
        }
    }
}