﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CatalogsApp.DataContracts;
using CatalogsApp.DataEntities;

namespace CatalogsApp.Data
{
    public abstract class RepositoryBase<TEntity> : IDisposable, IRepository<TEntity>
        where TEntity : class, IEntity
    {
        private readonly DbContext context;

        private bool disposed;

        protected RepositoryBase(DbContext context)
        {
            this.context = context;
        }

        public async Task<TEntity> FirstOrDefaultAsync(int id, CancellationToken cancellationToken)
        {
            return await context.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public IQueryable<TEntity> GetAll()
        {
            return context.Set<TEntity>().AsQueryable();
        }

        public Task<int> SaveAsync(TEntity entity)
        {
            if (entity.Id > 0 && entity.IsDeleted)
            {
                context.Entry(entity).State = EntityState.Deleted;
            }

            if (entity.Id > 0 && !entity.IsDeleted)
            {
                context.Entry(entity).State = EntityState.Modified;
            }

            context.SaveOrUpdate(entity);

            return Task.FromResult(entity.Id);
        }

        public Task DeleteAsync(TEntity entity)
        {
            return Task.FromResult(context.Set<TEntity>().Remove(entity));
        }

        public void CommitChanges()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
    }
}
