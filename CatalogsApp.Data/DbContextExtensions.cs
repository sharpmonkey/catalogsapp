﻿using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

namespace CatalogsApp.Data
{
    public static class DbContextExtensions
    {
        public static void SaveOrUpdate<T>(this ObjectContext context, T entity)
            where T : class
        {
            context.ObjectStateManager
                .TryGetObjectStateEntry(entity, out var stateEntry);

            var objectSet = context.CreateObjectSet<T>();
            if (stateEntry == null || stateEntry.EntityKey.IsTemporary)
            {
                objectSet.AddObject(entity);
            }

            else if (stateEntry.State == EntityState.Detached)
            {
                objectSet.Attach(entity);
                context.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
            }
        }

        public static ObjectContext GetObjectContext(this DbContext c)
        {
            return ((IObjectContextAdapter)c).ObjectContext;
        }

        public static void SaveOrUpdate<T>(this DbContext context, T entity)
            where T : class
        {
            context.GetObjectContext().SaveOrUpdate(entity);
        }
    }
}
