﻿using CatalogsApp.DataContracts.Products;
using CatalogsApp.DataEntities.Entities.Products;

namespace CatalogsApp.Data.Products
{
    public class ProductCatalogRepository : RepositoryBase<ProductCatalog>, IProductCatalogRepository
    {
        public ProductCatalogRepository(CatalogsAppDbContext context)
        : base(context)
        {
        }
    }
}
