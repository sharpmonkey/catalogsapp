﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using CatalogsApp.DataEntities.Entities.Products;

namespace CatalogsApp.Data
{
    public class CatalogsAppDbContext : DbContext
    {
        public DbSet<ProductCatalog> ProductCatalogs { get; set; }

        public CatalogsAppDbContext(string connString)
            : base(connString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
