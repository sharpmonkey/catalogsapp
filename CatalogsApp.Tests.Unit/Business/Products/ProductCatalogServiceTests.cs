﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using System.Threading.Tasks;
using Moq;
using CatalogApp.Business.Products;
using CatalogApp.Business.Products.Queries;
using CatalogsApp.DataContracts.Products;
using CatalogsApp.DataEntities.Entities.Products;
using static NUnit.Framework.Assert;

namespace CatalogsApp.Tests.Unit.Business.Products
{
    [TestFixture]
    public class ProductCatalogServiceTests
    {
        private ProductCatalogService productCatalogService;
        private Mock<IProductCatalogRepository> productCatalogRepositoryMock;

        [SetUp]
        public void SetUp()
        {
            productCatalogRepositoryMock = new Mock<IProductCatalogRepository>();
        }

        [TearDown]
        public void TearDown()
        {
            productCatalogRepositoryMock = null;
            productCatalogService = null;
        }

        [Test]
        public async Task ShouldGetProductCatalogById()
        {
            productCatalogRepositoryMock.Setup(x => x.FirstOrDefaultAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ProductCatalog
                {
                    Code = "T01",
                    Name = "Test",
                    Price = 1500,
                    IsConfirmed = true,
                    CreatedOn = DateTime.UtcNow,
                    LastUpdated = DateTime.UtcNow,
                    IsDeleted = false
                });

            productCatalogService = new ProductCatalogService(productCatalogRepositoryMock.Object);
            var productCatalog = await productCatalogService.GetProductCatalogByIdAsync(1, CancellationToken.None);

            That(productCatalog, Is.Not.Null);
            That(productCatalog.Code, Is.EqualTo("T01"));
        }

        [Test]
        public async Task ShouldFindProductCatalogs()
        {
            var items = new List<ProductCatalog>
            {
                new ProductCatalog
                {
                    Code = "T01",
                    Name = "Test-1",
                    Price = 1500,
                    IsConfirmed = true,
                    CreatedOn = DateTime.UtcNow,
                    LastUpdated = DateTime.UtcNow,
                    IsDeleted = false
                },
                new ProductCatalog
                {
                    Code = "T02",
                    Name = "Test-2",
                    Price = 1200.50m,
                    IsConfirmed = false,
                    CreatedOn = DateTime.UtcNow,
                    LastUpdated = DateTime.UtcNow,
                    IsDeleted = false
                }
            }.AsQueryable();

            productCatalogRepositoryMock.Setup(x => x.GetAll())
                .Returns(items);

            productCatalogService = new ProductCatalogService(productCatalogRepositoryMock.Object);
            var productCatalog = await productCatalogService.FindProductCatalogsAsync(new GetProductCatalogsQuery { Keyword = "T0" }, CancellationToken.None);

            That(productCatalog, Is.Not.Null);
            That(productCatalog.Count, Is.EqualTo(2));
            That(productCatalog.FirstOrDefault(x => x.Code == "T01"), Is.Not.Null);
            That(productCatalog.FirstOrDefault(x => x.Code == "T02"), Is.Not.Null);
        }
    }

    public static class MockExtensions
    {
        public static void SetupIQueryable<TRepository, TEntity>(this Mock<TRepository> mock, IQueryable<TEntity> queryable)
            where TRepository : class, IQueryable<TEntity>
        {
            mock.Setup(r => r.GetEnumerator()).Returns(queryable.GetEnumerator());
            mock.Setup(r => r.Provider).Returns(queryable.Provider);
            mock.Setup(r => r.ElementType).Returns(queryable.ElementType);
            mock.Setup(r => r.Expression).Returns(queryable.Expression);
        }
    }
}
