﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace CatalogsApp.Tests.Integration
{
    public abstract class TestBase
    {
        public static IServiceProvider ServiceProvider { get; set; }

        public TService GetService<TService>()
        {
            return ServiceProvider.GetService<TService>();
        }
    }
}
