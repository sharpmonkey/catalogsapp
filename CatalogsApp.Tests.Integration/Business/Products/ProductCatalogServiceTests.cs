﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CatalogApp.Business.Products;
using CatalogApp.Business.Products.Queries;
using CatalogsApp.DataEntities.Entities.Products;
using NUnit.Framework;
using static NUnit.Framework.Assert;

namespace CatalogsApp.Tests.Integration.Business.Products
{
    [TestFixture]
    public class ProductCatalogServiceTests : TestBase
    {
        private IProductCatalogService service;

        [SetUp]
        public void SetUp()
        {
            service = GetService<IProductCatalogService>();
        }

        [TearDown]
        public void TearDown()
        {
            service = null;
        }

        [Test]
        public async Task ShouldSaveProductCatalog()
        {
            var productCatalog = new ProductCatalog
            {
                Code = "T0-" + DateTime.UtcNow.Ticks,
                Name = "Test New",
                Price = 1500,
                IsConfirmed = true,
                CreatedOn = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow,
                IsDeleted = false
            };

            var id = await service.SaveProductCatalogAsync(productCatalog, CancellationToken.None);

            That(id, Is.GreaterThan(0));
        }

        [Test]
        public async Task ShouldUpdateProductCatalog()
        {
            var productCatalog = new ProductCatalog
            {
                Code = "T0-" + DateTime.UtcNow.Ticks,
                Name = "Test Update",
                Price = 750,
                IsConfirmed = true,
                CreatedOn = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow,
                IsDeleted = false
            };

            var id = await service.SaveProductCatalogAsync(productCatalog, CancellationToken.None);

            That(id, Is.GreaterThan(0));

            productCatalog.Name = "Test Update-" + DateTime.UtcNow.Ticks;
            await service.UpdateProductCatalogAsync(productCatalog, CancellationToken.None);

            var updatedProductCatalog = await service.GetProductCatalogByIdAsync(productCatalog.Id, CancellationToken.None);

            That(updatedProductCatalog, Is.Not.Null);
            That(updatedProductCatalog.Name, Is.EqualTo(productCatalog.Name));
        }

        [Test]
        public async Task ShouldFindProductCatalogs()
        {
            var productCatalog = new ProductCatalog
            {
                Code = "T0-" + DateTime.UtcNow.Ticks,
                Name = "Test Find",
                Price = 1500,
                IsConfirmed = true,
                CreatedOn = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow,
                IsDeleted = false
            };

            await service.SaveProductCatalogAsync(productCatalog, CancellationToken.None);

            var items = await service.FindProductCatalogsAsync(new GetProductCatalogsQuery(), CancellationToken.None);

            That(items, Is.Not.Null);
            That(items.Count, Is.GreaterThan(0));
        }

        [Test]
        public async Task ShouldExportProductCatalogs()
        {
            var productCatalog = new ProductCatalog
            {
                Code = "T0-" + DateTime.UtcNow.Ticks,
                Name = "Test Find",
                Price = 1500,
                IsConfirmed = true,
                CreatedOn = DateTime.UtcNow,
                LastUpdated = DateTime.UtcNow,
                IsDeleted = false
            };

            await service.SaveProductCatalogAsync(productCatalog, CancellationToken.None);

            var stream = await service.FindAndExportProductCatalogsAsync(new GetProductCatalogsQuery(), CancellationToken.None);

            That(stream, Is.Not.Null);
        }
    }
}
