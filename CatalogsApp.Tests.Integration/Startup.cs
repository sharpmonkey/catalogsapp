﻿using System.Configuration;
using CatalogApp.Business.Products;
using CatalogsApp.Data;
using CatalogsApp.Data.Products;
using CatalogsApp.DataContracts.Products;
using Microsoft.Extensions.DependencyInjection;

namespace CatalogsApp.Tests.Integration
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["CatalogsApp"].ConnectionString;

            services
                .AddScoped<IProductCatalogRepository>(x => new ProductCatalogRepository(
                    new CatalogsAppDbContext(connectionString)
                ))
                .AddScoped<IProductCatalogService>(x => new ProductCatalogService(
                    x.GetService<IProductCatalogRepository>()
                ));
        }
    }
}
