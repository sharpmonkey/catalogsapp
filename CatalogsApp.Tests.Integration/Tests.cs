﻿using System;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace CatalogsApp.Tests.Integration
{
    [SetUpFixture]
    public class Tests
    {
        public static IServiceProvider ServiceProvider { get; set; }

        [OneTimeSetUp]
        public virtual void RunBeforeAnyTests()
        {
            var services = new ServiceCollection();

            new Startup().ConfigureServices(services);

            ServiceProvider = services.BuildServiceProvider();

            TestBase.ServiceProvider = ServiceProvider;
        }

        [OneTimeTearDown]
        public virtual void RunAfterAllTests()
        {
            if (ServiceProvider != null)
            {
                if (ServiceProvider is IDisposable disposable)
                {
                    disposable.Dispose();
                }
            }
        }
    }
}
